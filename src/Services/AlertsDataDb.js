const AWS = require('aws-sdk');

const ALERT_DATA_DDB_TABLE_NAME = "alert_data_ddb";

const fetchAlertsData = (fromTime, toTime, ruleId, config) => {
    AWS.config.update(config);
    
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    // Define parameters for the query
    const params = {
        TableName: ALERT_DATA_DDB_TABLE_NAME,
        FilterExpression: '#rule_id = :rule_id AND #start_time BETWEEN :fromTime AND :toTime',
        ExpressionAttributeNames: {
            '#start_time': 'start_time',
            '#rule_id': 'rule_id'
        },
        ExpressionAttributeValues: {
            ':fromTime': fromTime,
            ':toTime': toTime,
            ':rule_id': ruleId
        },
        ScanIndexForward: false
    };
    
    return new Promise((resolve, reject) =>{
        dynamodb.scan(params, (err, data) => {
            if (err) {
                console.error('Unable to query - alert report query for all device data. Error:', JSON.stringify(err, null, 2));
                resolve([]);
            } else {
                console.log('Alert report query for all device succeeded:', JSON.stringify(data, null, 2));
                resolve(data.Items);
            }
        });
    });
}


const fetchAlertDetailsData = (fromTime, toTime, ruleId, deviceId, config) => {
    AWS.config.update(config);
    
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    // Define parameters for the query
    const params = {
        TableName: ALERT_DATA_DDB_TABLE_NAME,
        KeyConditionExpression: '#deviceId = :deviceId AND #start_time BETWEEN :fromTime AND :toTime',
        FilterExpression: '#rule_id = :rule_id',
        ExpressionAttributeNames: {
            '#deviceId': 'device_id',
            '#start_time': 'start_time',
            '#rule_id': 'rule_id'
        },
        ExpressionAttributeValues: {
            ':deviceId': deviceId,
            ':fromTime': fromTime,
            ':toTime': toTime,
            ':rule_id': ruleId
        },
        ScanIndexForward: false
    };
    
    return new Promise((resolve, reject) =>{
        dynamodb.query(params, (err, data) => {
            if (err) {
                console.error('Unable to query - alert details report query for single device data. Error:', JSON.stringify(err, null, 2));
                resolve([]);
            } else {
                console.log('Alert detail report query for single device succeeded:', JSON.stringify(data, null, 2));
                resolve(data.Items);
            }
        });
    });
}

module.exports = {
    fetchAlertsData: fetchAlertsData,
    fetchAlertDetailsData: fetchAlertDetailsData
};