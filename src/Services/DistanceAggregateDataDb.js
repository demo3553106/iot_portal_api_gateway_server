const AWS = require('aws-sdk');

const DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME = 'distance_aggregate_ddb';

const fetchDistanceAggregateForDeviceId = (fromTime, toTime, timeFrame, deviceId, config) => {
    AWS.config.update(config);

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    // Define parameters for the query
    const params = {
        TableName: DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME,
        KeyConditionExpression: '#deviceId = :deviceId AND #timestamp BETWEEN :fromTime AND :toTime',
        FilterExpression: '#time_frame = :time_frame',
        ExpressionAttributeNames: {
            '#deviceId': 'device_id',
            '#timestamp': 'timestamp',
            '#time_frame': 'time_frame'
        },
        ExpressionAttributeValues: {
            ':deviceId': deviceId,
            ':fromTime': fromTime,
            ':toTime': toTime,
            ':time_frame': timeFrame
        },
        ScanIndexForward: false
    };
    
    return new Promise((resolve, reject) =>{
        dynamodb.query(params, (err, data) => {
            if (err) {
                console.error('Unable to query - distance report query for single device data. Error:', JSON.stringify(err, null, 2));
                resolve([]);
            } else {
                console.log('Distance report query for single device succeeded:', JSON.stringify(data, null, 2));
                // const result = data.Items.map(item => {
                //     return JSON.parse(item.data);
                // })
                resolve(data.Items);
            }
        });
    });
}

const fetchDistanceAggregateForAllDevice = (fromTime, toTime, timeFrame, config) => {
    AWS.config.update(config);

    const dynamodb = new AWS.DynamoDB.DocumentClient();

    // Define parameters for the query
    const params = {
        TableName: DISTANCE_AGGREGATE_DATA_DDB_TABLE_NAME,
        FilterExpression: '#time_frame = :time_frame AND #timestamp BETWEEN :fromTime AND :toTime',
        ExpressionAttributeNames: {
            '#timestamp': 'timestamp',
            '#time_frame': 'time_frame'
        },
        ExpressionAttributeValues: {
            ':fromTime': fromTime,
            ':toTime': toTime,
            ':time_frame': timeFrame
        },
        ScanIndexForward: false
    };
    
    return new Promise((resolve, reject) =>{
        dynamodb.scan(params, (err, data) => {
            if (err) {
                console.error('Unable to query - distance report for all devices. Error:', JSON.stringify(err, null, 2));
                resolve([]);
            } else {
                console.log('Distance report query for all devices succeeded:', JSON.stringify(data, null, 2));
                resolve(data.Items);
            }
        });
    });
}


module.exports = {
    fetchDistanceAggregateForDeviceId: fetchDistanceAggregateForDeviceId,
    fetchDistanceAggregateForAllDevice: fetchDistanceAggregateForAllDevice
};