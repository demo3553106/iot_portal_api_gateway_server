const mysql = require('mysql');


const createConfigDb = (config) => {

    const db = mysql.createConnection(config);
    
    // Connect to the database
    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
        console.log('Connected to config db.');
    });

    db.query(`
        CREATE TABLE  IF NOT EXISTS device_config_table (
            device_id INT AUTO_INCREMENT PRIMARY KEY,
            serial_no VARCHAR(255) NOT NULL,
            asset_name VARCHAR(255) NOT NULL,
            asset_no VARCHAR(255) NOT NULL,
            description VARCHAR(255) NOT NULL
        )
        `, err => {
        if (err) {
            console.error('Error creating table:', err);
            return;
        }
        console.log('Table initial setup is completed');
    });
}

const addDevice = async (data, config) => {

    const db = mysql.createConnection(config);

    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
    });

    const sqlQuery = 'INSERT INTO device_config_table (serial_no, asset_name, asset_no, description) VALUES (?, ?, ?, ?)';

    return await db.query(sqlQuery, [data.serial_no, data.asset_name, data.asset_no, data.description, ], (err, results) => {
        if (err) {
          console.error('Error executing query:', err);
          return {};
        }
        console.log('Query results:', results);
        return results;
    });   
}

const fetchDeviceDetails = async (serialNo, config) => {

    const db = mysql.createConnection(config);
    
    // Connect to the database
    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
    });

    return new Promise((resolve, reject) =>{
        db.query(`SELECT * FROM device_config_table WHERE serial_no = ?`, [serialNo], (err, results) => {
            if (err) {
            console.error('Error executing query:', err);
                resolve({});
            }
            console.log('Query results:', results);
            resolve(results);
        }); 
    });  
}
const fetchDeviceDetailsById = async (deviceId, config) => {

    const db = mysql.createConnection(config);
    
    // Connect to the database
    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
    });

    return new Promise((resolve, reject) =>{
        db.query(`SELECT * FROM device_config_table WHERE device_id = ?`, [deviceId], (err, results) => {
            if (err) {
            console.error('Error executing query:', err);
                resolve({});
            }
            console.log('Query results:', results);
            resolve(results[0]);
        }); 
    });  
}

const fetchAllDeviceDetails = async (config) => {

    const db = mysql.createConnection(config);
    
    // Connect to the database
    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
    });

    return new Promise((resolve, reject) =>{
        db.query(`SELECT * FROM device_config_table`, (err, results) => {
            if (err) {
            console.error('Error executing query:', err);
                resolve({});
            }
            console.log('Query results:', results);
            resolve(results);
        }); 
    });  
}

const deleteDeviceId = async (deviceId, config) => {

    const db = mysql.createConnection(config);
    
    // Connect to the database
    db.connect(err => {
        if (err) {
            console.error('Error connecting to config db:', err);
            return;
        }
    });

    const sql = 'DELETE FROM device_config_table WHERE device_id = ?';
    return new Promise((resolve, reject) =>{
        db.query(sql, [deviceId], (error, results, fields) => {
            if (error) {
                console.error('Error deleting data:', error);
                resolve(false);
            }
            console.log('Data deleted successfully');
            resolve(true);  
        });
    });
}


module.exports = {
    createConfigDb: createConfigDb,
    addDevice: addDevice,
    fetchDeviceDetails: fetchDeviceDetails,
    deleteDeviceId: deleteDeviceId,
    fetchAllDeviceDetails: fetchAllDeviceDetails,
    fetchDeviceDetailsById: fetchDeviceDetailsById
}
