const {fetchDistanceAggregateForDeviceId, fetchDistanceAggregateForAllDevice} = require('../Services/DistanceAggregateDataDb');

const getDistanceTravelledReports = async ( fromTime, toTime, timeFrame, deviceId, config) => {
    let result = [];
    if(deviceId){
        result = await fetchDistanceAggregateForDeviceId(fromTime, toTime, timeFrame, deviceId, config);
    }else {
        result = await fetchDistanceAggregateForAllDevice(fromTime, toTime, timeFrame, config);
    }
    return result;
}

module.exports = {
    getDistanceTravelledReports: getDistanceTravelledReports
}