
const {fetchAllDeviceDetails, fetchDeviceDetailsById} = require('../Services/DeviceCongfigDb');

const getDevices = async (config) => {
    return await fetchAllDeviceDetails(config);
}

const getDevicesById = async (deviceId, config) => {
    return await fetchDeviceDetailsById(deviceId, config);
}

module.exports = {
    getDevices: getDevices,
    getDevicesById: getDevicesById
}