const {fetchAlertsData, fetchAlertDetailsData} = require('../Services/AlertsDataDb');

const getAlertsReports  = async (date, timeFrame, ruleId, config) => {
    let fromTime = (new Date(date));
    fromTime.setMinutes(0);
    fromTime.setSeconds(0);
    fromTime.setMilliseconds(0);
    if(timeFrame != 'hour') fromTime.setHours(0);
    let toTime = fromTime.getTime();
    if(timeFrame == 'hour') toTime += 60*60*1000;
    if(timeFrame == 'day') toTime += 24*60*60*1000;
    const result = await fetchAlertsData(fromTime.getTime(), toTime, ruleId, config);

    let deviceWiseCout = {};
    result.forEach((item) => {
        if(!deviceWiseCout[item.device_id])  deviceWiseCout[item.device_id] = 0;
        deviceWiseCout[item.device_id]++;
    });
    const keys = Object.keys(deviceWiseCout);
    const response = keys.map(key => {
        return { device_id: key, violation_count: deviceWiseCout[key]}
    });
    return {rule_id: ruleId, violations: response};
}

const getAlertDetailReports = async (date, timeFrame, ruleId, deviceId, config ) => {
    let fromTime = (new Date(date));
    fromTime.setMinutes(0);
    fromTime.setSeconds(0);
    fromTime.setMilliseconds(0);
    if(timeFrame != 'hour') fromTime.setHours(0);
    let toTime = fromTime.getTime();
    if(timeFrame == 'hour') toTime += 60*60*1000;
    if(timeFrame == 'day') toTime += 24*60*60*1000;
    const result = await fetchAlertDetailsData(fromTime.getTime(), toTime, ruleId, deviceId, config);
    return {rule_id: ruleId, device_id: deviceId, violations: result};
}

module.exports = {
    getAlertsReports :getAlertsReports,
    getAlertDetailReports: getAlertDetailReports
}