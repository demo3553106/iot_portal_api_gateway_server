'use strict';

const dotEnvFlow = require('dotenv-flow');
const express = require('express');

const {getDevices, getDevicesById} = require('./ApiHandlers/GetDevices');
const {getDistanceTravelledReports} = require('./ApiHandlers/GetDistanceReport');
const {getAlertsReports, getAlertDetailReports} = require('./ApiHandlers/GetAlertsReport');


// load .env file
dotEnvFlow.config({ node_env: process.env });

// load applicable / allowed environment variables
const {
	IOT_SERVER_PORT,
	AWS_REGION,
	AWS_ACCESS_KEY,
	AWS_SECRET_KEY,
    RDS_HOST_END_POINT,
	RDS_PORT,
	RDS_USER_NAME,
	RDS_PASSWORD,
	RDS_DB_NAME
} = process.env;

const AWS_CONFIG = { region: AWS_REGION, credentials:{accessKeyId : AWS_ACCESS_KEY, secretAccessKey: AWS_SECRET_KEY } };

const RDS_CONFIG = {
    host: RDS_HOST_END_POINT,
    port: RDS_PORT,
    user: RDS_USER_NAME,
    password: RDS_PASSWORD,
    database: RDS_DB_NAME
};


// Create the server
const app = express();


app.get('/', (req, res) => {
  res.send('IOT Portal Dashboad');
});

//get device config details
app.get('/api/devices', async (req, res) => { console.log(req.param);
    const result = await getDevices(RDS_CONFIG);
    res.json(result);
});

app.get('/api/device/:device_id', async (req, res) => { console.log(req.param);
    const device_id = parseInt(req.params.device_id);
    const result = await getDevicesById(device_id, RDS_CONFIG);
    res.json(result);
});

//create a device config
app.post('/api/devices', (req, res) => {
    // TODO
    res.json({});
});

// get currenet status/parameter details of the device
app.get('/api/status/:device_id', async (req, res) => {
    const device_id = parseInt(req.params.device_id);
    const status = {}; //TODO

    if (!status) {
        return res.status(404).json({ error: 'Device status not found' });
    }

    res.json(status);
});


// get report of all devices. e.g /api/reports/distance?fromDate=2024-01-01&toDate=2024-02-14&timeFrame=hour&deviceId=1
app.get('/api/reports/distance', async (req, res) => {
    const fromDate = (new Date(req.query.fromDate)).getTime();
    const toDate = (new Date(req.query.toDate)).getTime();
    const timeFrame = req.query.timeFrame;
    let deviceId = parseInt(req.query.deviceId);
    
    if(!deviceId){
        deviceId = 0;   // zero means fetch all device data in the report
    }

    const result = await getDistanceTravelledReports(fromDate, toDate, timeFrame, deviceId, AWS_CONFIG);

    res.json(result);
});

// get report of all devices. e.g /api/reports/alerts?date=2024-02-13&timeFrame=day&ruleId=1
app.get('/api/reports/alerts', async (req, res) => {
    const date = (new Date(req.query.date)).getTime();
    const timeFrame = req.query.timeFrame;
    const ruleId = parseInt(req.query.ruleId);

    const result = await getAlertsReports(date, timeFrame, ruleId, AWS_CONFIG);

    res.json(result);
});

/* 
    get report of single device alerts: 
    e.g:
        /api/reports/alert_detail?date=2024-02-13&timeFrame=day&ruleId=1&deviceId=1
        /api/reports/alert_detail?date=2024-02-13%2023:00&timeFrame=hour&ruleId=1&deviceId=1
*/ 

app.get('/api/reports/alert_detail', async (req, res) => {
    const date = (new Date(req.query.date)).getTime();
    const timeFrame = req.query.timeFrame;
    const ruleId = parseInt(req.query.ruleId);
    const deviceId = parseInt(req.query.deviceId);

    const result = await getAlertDetailReports(date, timeFrame, ruleId, deviceId, AWS_CONFIG);

    res.json(result);
});

// Start the server
app.listen(IOT_SERVER_PORT, async () => {
    console.log(`Server is running on http://localhost:${IOT_SERVER_PORT}`);
});


// createConfigDb(RDS_CONFIG);
// addDevice({
// 	serial_no: 'XYZ0000005',
//     asset_no: '12OR54215',
//     asset_name: 'Truck-04',
//     group_id: 1,
//     description: 'TATA truck deployed in bangalore'
// }, RDS_CONFIG);
